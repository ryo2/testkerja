from django.urls import path
from .views import CreateUserView, ListUserView, UserDetailView

urlpatterns = [
    path('/', CreateUserView.as_view()),
    path('/<int:limit>/<int:offset>', ListUserView.as_view()),
    path('/<int:id>', UserDetailView.as_view())
]
