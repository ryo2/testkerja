import json
from django.db import IntegrityError
from django.http import JsonResponse, HttpResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.hashers import make_password
from django.forms.models import model_to_dict

from user_management.models import UserCustom
def bad_request():
            return JsonResponse({"status":"fail", "message":"bad request"}, status=400, safe=False)

class CreateUserView(View):
    @csrf_exempt
    def post(self,request,*args,**kwargs):
        if request.body:
            data = json.loads(request.body)
            try:
                if(len(data['username'])<3 or \
                    len(data['password'])<7 or \
                    len(data['nama'])<3):  
                    return bad_request()
                user = UserCustom()
                user.nama = data['nama']
                user.password = make_password(data['password'])
                user.username = data['username']
                try :
                    user.save()
                except IntegrityError:
                    return JsonResponse({"status":"fail", "message":"user already exist"}, status=200, safe=False)
                return JsonResponse({}, safe=False)
            except KeyError:
                return bad_request()
        else:
            return bad_request()

class ListUserView(View):
    @csrf_exempt
    def get(self,request,*args,**kwargs):
        offset = kwargs['offset']
        limit = kwargs['limit']
        if offset < 0:
            offset = 0
        if limit < 0:
            limit = 1
        users_queryset = UserCustom.objects.all()[offset:offset+limit]
        users = [x for x in users_queryset.values()]
        return JsonResponse(users, status=200, safe=False)

class UserDetailView(View):
    @csrf_exempt
    def get(self,request,*args,**kwargs):
        try:
            user = UserCustom.objects.get(pk=kwargs['id'])
        except UserCustom.DoesNotExist:
            user = None
        if not user:
            return JsonResponse({"status":"fail", "message":"user doesn't exist"}, status=200, safe=False)
        user = model_to_dict(user)
        return JsonResponse(user, status=200, safe=False)
    def delete(self,request,*args,**kwargs):
        try:
            user = UserCustom.objects.get(pk=kwargs['id'])
        except UserCustom.DoesNotExist:
            user = None
        if not user:
            return JsonResponse({"status":"fail", "message":"user doesn't exist"}, status=200, safe=False)
        user.delete()
        return JsonResponse({}, status=200, safe=False)
    def put(self,request,*args,**kwargs):
        if request.body:
            return self.post(request,*args,**kwargs)
        else:
            return bad_request()
    def patch(self,request,*args,**kwargs):
        if request.body:
            return self.post(request,*args,**kwargs)
        else:
            return bad_request()
    def post(self,request,*args,**kwargs):
        try:
            user = UserCustom.objects.get(pk=kwargs['id'])
        except UserCustom.DoesNotExist:
            user = None
        if not user:
            return JsonResponse({"status":"fail", "message":"user doesn't exist"}, status=200, safe=False)
        if request.body:
            data = json.loads(request.body)
            try:
                user = UserCustom.objects.get(pk=kwargs['id'])
                if "username"in data:
                    if len(data['username'])<3 and len(data['username'])>0: 
                        return bad_request()
                    user.username=data["username"]
                if "password"in data:
                    if len(data['password'])<7 and len(data['password'])>0: 
                        return bad_request()
                    user.password=make_password(data["username"])
                if "nama"in data:
                    if len(data['nama'])<3 and len(data['nama'])>0:  
                        return bad_request()
                    user.nama=data["nama"]
                try :
                    user.save()
                except IntegrityError:
                    return JsonResponse({"status":"fail", "message":"user already exist"}, status=200, safe=False)
                return JsonResponse({}, safe=False)
            except KeyError:
                return bad_request()
            user = model_to_dict(user)
            return JsonResponse(user, status=200, safe=False)
        else:
            user.delete()
        return JsonResponse({}, status=200, safe=False)
