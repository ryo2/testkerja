import React from 'react'
import CIcon from '@coreui/icons-react'

const _nav =  [
 
  {
    _tag: 'CSidebarNavItem',
    name: 'User Management',
    to: '/users',
    icon: 'cil-people',
  }
]

export default _nav
